let AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
const uuid = require('uuid');
const responder = require('./responder');

function response(statusCode, message) {
  return {
    statusCode: statusCode,
    body: JSON.stringify(message)
  };
}

exports.handler = async (event, context, callback) => {
    debugger;    

    var userId = uuid.v1();
    var userName = event.UserName;
    var userMessage = event.UserMessage;
    var createdDate = Date.now();

    console.log(event.UserName);
    console.log(event.UserMessage);
    console.log(createdDate);
    console.log(userId);

    const entry = {
        UserId: userId,
        UserName: userName,
        UserMessage: userMessage,
        DateCreated: createdDate
    };

    return ddb.put({
        TableName: "sigmaGuestBook",
        Item: entry
    }).promise()
    .then(() => {
        callback(null, response(201, entry));
    })
    .catch(err => response(null, response(err.statusCode, err)));
    // .promise()
    //     .then(data => {
    //         debugger;
    //         console.log("Success");
    //         // your code goes here
    //         return callback(responder.success({ "Message": "Success" }));
    //     })
    //     .catch(err => {
    //         debugger;
    //         console.log("Error");
    //         // error handling goes here
    //         return callback(responder.internalServerError({ "error": "An error occurred" }));
    //     });
};