const AWS = require( 'aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const responder = require('./responder');

exports.handler = async (event, context, callback) => {
    debugger;
    // return db.scan({
    //     TableName: "sigmaGuestBook"
    // }).promise()
    // .then(res => {
    //     callback(null, responder.success(res.Items.sort(DateCreated)));
    // })
    // .catch(err => callback(null, responder.internalServerError({"error": err})));

    try{
        var res = db.scan({
            TableName: "sigmaGuestBook"
        });
        callback(null, responder.success(res.Items.sort(DateCreated)));
        
    } catch (err) {
        callback(null, responder.internalServerError({"error": err}));
    }
};